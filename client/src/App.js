import { useEffect, useState } from "react";
import './App.css';
import Table from "./components/Table";
import Modal from "./components/Modal";

function App() {
  const API = "http://localhost:4000/api/v1/";
  const PLAYERS = "players/";
  const [players, setPlayers] = useState([]);
  const [loading, setLoading] = useState(true);
  const [inputs, setInputs] = useState({});
  const [idForSave, setIdForSave] = useState(0);
  const [action, setAction] = useState('');
  const [showInputPassword, setShowInputPassword] = useState(true);

  const handleSubmit = (event) => {
    event.preventDefault();
    let queryParameter = "";
    let username = "";
    let email = "";
    let experience = "";
    let level = "";

    if (inputs.floatingInputUsername) {
      username = inputs.floatingInputUsername.trim();

      if (username) {
        queryParameter += "username=" + username + "&";
      }
    }

    if (inputs.floatingInputEmail) {
      email = inputs.floatingInputEmail.trim();

      if (email) {
        queryParameter += "email=" + email + "&";
      }
    }

    if (inputs.floatingInputExperience) {
      experience = inputs.floatingInputExperience.trim();

      if (experience) {
        queryParameter += "experience=" + experience + "&";
      }
    }

    if (inputs.floatingInputLevel) {
      level = inputs.floatingInputLevel.trim();

      if (level) {
        queryParameter += "lvl=" + level;
      }
    }

    if (queryParameter) {
      queryParameter = queryParameter.substring(0, queryParameter.length - 1);
      queryParameter = "?" + queryParameter;
    }

    fetchPlayers(queryParameter);
  }

  const handleReset = (event) => {
    setInputs(values => ({
      ...values,
      floatingInputUsername: '',
      floatingInputEmail: '',
      floatingInputExperience: '',
      floatingInputLevel: ''
    }));

    fetchPlayers();
  }

  const handleChange = (event) => {
    const id = event.target.id;
    const value = event.target.value;
    setInputs(values => ({ ...values, [id]: value }));
  }

  useEffect(() => {
    fetchPlayers();

    let exampleModal = document.getElementById('exampleModal');
    // let modalBodyInputUsername = exampleModal.querySelector('.modal-body #floatingModalInputUsername');

    exampleModal.addEventListener('show.bs.modal', (event) => {
      // Button that triggered the modal
      let button = event.relatedTarget;

      // Extract info from data-bs-* attributes
      setAction(button.getAttribute('data-bs-whatever'));

      // If necessary, you could initiate an AJAX request here
      // and then do the updating in a callback.
      if (button.getAttribute('data-bs-whatever') === "Edit") {
        setIdForSave(button.getAttribute('data-bs-id'));
        fetchPlayer(button.getAttribute('data-bs-id'));
      } else {
        setIdForSave(0);
        setInputValue({
          id: idForSave,
          username: "",
          email: "",
          experience: 0
        });
      }

      // modalBodyInputUsername.focus();
    });
  }, []);

  const setInputValue = (player) => {
    setInputs(values => ({
      ...values,
      floatingModalInputUsername: player.username,
      floatingModalInputEmail: player.email,
      floatingModalInputPassword: '',
      floatingModalInputExperience: player.experience
    }));

    if (player.id) {
      setShowInputPassword(false);
    } else {
      setShowInputPassword(true);
    }
  }

  const fetchPlayer = async (params = '') => {
    const response = await fetch(
      API + PLAYERS + params, { method: 'GET' }
    );

    if (!response.ok) {
      alert(`HTTP error! status: ${response.status}`);
    }

    const data = await response.json();
    if (data.result.toUpperCase() === 'SUCCESS') {
      setInputValue(data.data);
    } else {
      alert(data.message);
    }
  }

  const fetchPlayers = async (params = '') => {
    let apiFetch = API + PLAYERS;

    if (params) {
      apiFetch = apiFetch.substring(0, apiFetch.length - 1);
      apiFetch += params;
    }

    const response = await fetch(
      apiFetch, { method: 'GET' }
    );

    if (!response.ok) {
      setLoading(false);
      alert(`HTTP error! status: ${response.status}`);
    }

    const data = await response.json();
    if (data.result.toUpperCase() === 'SUCCESS') {
      setPlayers(data.data);
      setLoading(false);
    }
  }

  const savePlayer = async () => {
    let apiSave = API + PLAYERS;
    let method = "post";
    let body = {
      username: inputs.floatingModalInputUsername,
      email: inputs.floatingModalInputEmail,
      experience: inputs.floatingModalInputExperience
    };

    if (idForSave) {
      apiSave += idForSave;
      method = "put";
    } else {
      body.password = inputs.floatingModalInputPassword;
    }

    const response = await fetch(
      apiSave,
      {
        method: method,
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
        body: JSON.stringify(body)
      });

    if (!response.ok) {
      alert(`HTTP error! status: ${response.status}`);
    }

    const data = await response.json();
    if (data.result.toUpperCase() === 'SUCCESS') {
      fetchPlayers();
    } else {
      alert(data.message);
    }
  }

  const deletePlayer = async (id, username) => {
    if (window.confirm("Are you sure you want to delete player '" + username + "'?")) {
      const response = await fetch(
        API + PLAYERS + id, { method: 'delete' }
      );

      if (!response.ok) {
        alert(`HTTP error! status: ${response.status}`);
      }

      const data = await response.json();
      alert(data.message);
      if (data.result.toUpperCase() === 'SUCCESS') {
        fetchPlayers();
      }
    }
  }

  return (
    <div className="container">
      <Table players={players} deletePlayer={deletePlayer} loading={loading} inputs={inputs} handleChange={handleChange}
        handleSubmit={handleSubmit} handleReset={handleReset} />

      <Modal savePlayer={savePlayer} inputs={inputs} handleChange={handleChange} action={action} showInputPassword={showInputPassword} />
    </div>
  );
}

export default App;
