function ModalFooter({ savePlayer }) {
    return (
        <div className="modal-footer">
            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={() => savePlayer()}>Save</button>
        </div>
    );
}

export default ModalFooter;