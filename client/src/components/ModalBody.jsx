function ModalBody({ inputs, handleChange, showInputPassword }) {
    return (
        <div className="modal-body">
            <form>
                <div className="form-floating mb-3">
                    <input type="text" className="form-control" id="floatingModalInputUsername" name="username" placeholder="Username"
                        value={inputs.floatingModalInputUsername || ""} onChange={handleChange} required />
                    <label htmlFor="floatingModalInputUsername">Username</label>
                </div>

                <div className="form-floating mb-3">
                    <input type="email" className="form-control" id="floatingModalInputEmail" name="email"
                        placeholder="name@example.com" value={inputs.floatingModalInputEmail || ""} onChange={handleChange} required />
                    <label htmlFor="floatingModalInputEmail">Email</label>
                </div>
                <div className="form-floating mb-3">
                    <input type="password" className={"form-control " + (showInputPassword ? "" : "hidden")} id="floatingModalInputPassword" name="password"
                        placeholder="Password" value={inputs.floatingModalInputPassword || ""} onChange={handleChange} required />
                    <label htmlFor="floatingModalInputPassword">Password</label>
                </div>

                <div className="form-floating mb-3">
                    <input type="number" className="form-control" id="floatingModalInputExperience" name="duration"
                        placeholder="Experience" value={inputs.floatingModalInputExperience || ""} onChange={handleChange} />
                    <label htmlFor="floatingModalInputExperience">Experience</label>
                </div>

            </form>
        </div>
    );
}

export default ModalBody;