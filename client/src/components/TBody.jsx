function TBody({ loading, players, deletePlayer }) {
    return (
        <>
            {
                !loading ? (
                    <tbody>
                        {players.map((player, index) => (
                            <tr id={player.id} key={player.id}>
                                <th scope="row">
                                    {index + 1}
                                </th>
                                <td>
                                    {player.username}
                                </td>
                                <td>
                                    {player.email}
                                </td>
                                <td>
                                    {player.experience}
                                </td>
                                <td>
                                    {player.lvl}
                                </td>
                                <td>
                                    {player.createdAt}
                                </td>
                                <td>
                                    {player.updatedAt}
                                </td>
                                <td>
                                    <button type="button" className="btn btn-warning" data-bs-toggle="modal" data-bs-target="#exampleModal"
                                        data-bs-whatever="Edit" data-bs-id={player.id}><i className="bi-pencil"></i></button>
                                    &nbsp;
                                    <button type="button" className="btn btn-danger" onClick={() => deletePlayer(player.id, player.username)} ><i
                                        className="bi-trash"></i></button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                ) : (<tbody>
                    <tr>
                        <th scope="row">...</th>
                        <td>...</td>
                        <td>...</td>
                        <td>...</td>
                        <td>...</td>
                        <td>...</td>
                        <td>...</td>
                        <td>...</td>
                    </tr>
                </tbody>)
            }
        </>
    );
}

export default TBody;