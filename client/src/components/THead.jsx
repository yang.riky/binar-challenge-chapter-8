function THead() {
    return (
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <th scope="col">Experience</th>
                <th scope="col">Level</th>
                <th scope="col">Created At</th>
                <th scope="col">Updated At</th>
                <th scope="col"></th>
            </tr>
        </thead>
    );
}

export default THead;