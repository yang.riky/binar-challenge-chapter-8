function ModalHeader({ action }) {
    return (
        <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">{action + " Player"}</h5>
            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
    );
}

export default ModalHeader;