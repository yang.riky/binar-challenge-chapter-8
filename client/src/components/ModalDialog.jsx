import ModalContent from "./ModalContent";

function ModalDialog({ savePlayer, inputs, handleChange, action, showInputPassword }) {
    return (
        <div className="modal-dialog">
            <ModalContent savePlayer={savePlayer} inputs={inputs} handleChange={handleChange} action={action} showInputPassword={showInputPassword} />
        </div>
    );
}

export default ModalDialog;