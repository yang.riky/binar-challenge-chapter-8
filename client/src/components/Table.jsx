import TableCaption from "./TableCaption";
import TBody from "./TBody";
import THead from "./THead";

function Table({ players, deletePlayer, loading, inputs, handleChange, handleSubmit, handleReset }) {
    return (
        <div className="table-responsive">
            <table className="table table-hover caption-top align-middle">
                <TableCaption inputs={inputs} handleChange={handleChange} handleSubmit={handleSubmit} handleReset={handleReset} />
                <THead />
                <TBody players={players} loading={loading} deletePlayer={deletePlayer} />
            </table>
        </div>
    );
}

export default Table;