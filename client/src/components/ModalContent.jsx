import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";
import ModalHeader from "./ModalHeader";

function ModalContent({ savePlayer, inputs, handleChange, action, showInputPassword }) {
    return (
        <div className="modal-content">
            <ModalHeader action={action} />
            <ModalBody inputs={inputs} handleChange={handleChange} showInputPassword={showInputPassword} />
            <ModalFooter savePlayer={savePlayer} />
        </div>
    );
}

export default ModalContent;