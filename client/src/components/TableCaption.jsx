function TableCaption({ inputs, handleChange, handleSubmit, handleReset }) {
    return (
        <caption>List of players&nbsp;
            <button type="button" className="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModal"
                data-bs-whatever="Add"><i className="bi-plus-circle"></i></button>

            <form onSubmit={handleSubmit} onReset={handleReset} className="row row-cols-lg-auto g-3 align-items-center">
                <div className="col-12 form-floating">
                    <input type="text" className="form-control" id="floatingInputUsername" name="username" placeholder="Username"
                        value={inputs.floatingInputUsername || ""} onChange={handleChange} autoFocus />
                    <label htmlFor="floatingInputUsername">Username</label>
                </div>

                <div className="col-12 form-floating">
                    <input type="email" className="form-control" id="floatingInputEmail" name="email"
                        placeholder="name@example.com" value={inputs.floatingInputEmail || ""} onChange={handleChange} />
                    <label htmlFor="floatingInputEmail">Email</label>
                </div>

                <div className="col-12 form-floating">
                    <input type="number" className="form-control" id="floatingInputExperience" name="duration"
                        placeholder="Experience" value={inputs.floatingInputExperience || ""} onChange={handleChange} />
                    <label htmlFor="floatingInputExperience">Experience</label>
                </div>

                <div className="col-12 form-floating">
                    <input type="number" className="form-control" id="floatingInputLevel" name="duration"
                        placeholder="Level" value={inputs.floatingInputLevel || ""} onChange={handleChange} />
                    <label htmlFor="floatingInputLevel">Level</label>
                </div>

                <div className="col-12 form-floating">
                    <button type="submit" className="btn btn-primary"><i className="bi bi-search"></i></button>
                    &nbsp;
                    <button type="reset" className="btn btn-danger"><i className="bi bi-x-circle"></i></button>
                </div>
            </form>

        </caption>
    );
}

export default TableCaption;