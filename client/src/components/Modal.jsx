import ModalDialog from "./ModalDialog";

function Modal({ savePlayer, inputs, handleChange, action, showInputPassword }) {
    return (
        <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <ModalDialog savePlayer={savePlayer} inputs={inputs} handleChange={handleChange} action={action} showInputPassword={showInputPassword} />
        </div>
    );
}

export default Modal;